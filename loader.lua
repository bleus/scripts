local scriptURL = string.format('https://gitlab.com/bleus/scripts/-/raw/main/%d/script.lua', game.GameId);

local _, res = pcall(game.HttpGet, game, scriptURL);
if res then loadstring(res)() end;
