--[[
    by sukaretto#8874(https://discord.gg/PFuQMWMhQQ)
--]]

assert(game.PlaceId ~= 4809447488, 'Execute the script in an actual dungeon.');

local game = game;
local httpGet = game.HttpGet;

local quick = loadstring(httpGet(game, 'https://raw.githubusercontent.com/Belkworks/quick/master/init.lua'))();
local broom = loadstring(httpGet(game, 'https://raw.githubusercontent.com/Belkworks/broom/master/init.lua'))();
local pathing = loadstring(httpGet(game, 'https://raw.githubusercontent.com/V3N0M-Z/RBLX-SimplePath/main/src/SimplePath.lua'))();

-- services.
local s = quick.Service;
local workspace = s.Workspace;
local replicatedStorage = s.ReplicatedStorage;
local runService = s.RunService;

-- indexing.
local cf, v3 = CFrame.new, Vector3.new;

local findFirstChild, waitForChild = game.FindFirstChild, game.WaitForChild;
local getChildren, getPartsByRadius = game.GetChildren, workspace.GetPartBoundsInRadius;

local create, destroy = Instance.new, game.Destroy;
local fire, fireServer = create'BindableEvent'.Fire, create'RemoteEvent'.FireServer;

local string_find = string.find;
local string_format = string.format;

local c = game.Loaded;
local connect, cwait = c.Connect, c.Wait;
local render, step = runService.RenderStepped, runService.Stepped;

-- client.
local client = quick.User;
local character = client.Character or cwait(client.CharacterAdded);
local root = waitForChild(character, 'HumanoidRootPart');
local humanoid = waitForChild(character, 'Humanoid');

local distanceFromCharacter = client.DistanceFromCharacter;
local characterPathing = pathing.new(character, {
    Costs = {
        Water = math.huge,
        Neon = math.huge
    }}, {
    TIME_VARIANCE = 0.01,
    COMPARISON_CHECKS = 2,
    JUMP_WHEN_STUCK = false
});
    
-- things.
local map = workspace.Map;
local projectiles = workspace.Projectiles;

-- weapon calculation.
local equippedWeapon, blacklistedWeaponTypes = { hitDelay = 1.2, range = 6 }, {
    ['Shield'] = true,
    ['Wand'] = true,
    ['Staff'] = true,
    ['Spell'] = true,
    ['Book'] = true,
    ['Bow'] = true,
    ['Crossbow'] = true
} do
    local function calculate(weapon)
        local weaponType = weapon.ItemType.Value;
        if blacklistedWeaponTypes[weaponType] then return end;
        
        local weaponTypeFolder = replicatedStorage.ItemData[weaponType];
        local weaponData = quick.find(getChildren(weaponTypeFolder), function(w) return w.Name == weapon.ItemID.Value end);

        if weaponData then
            equippedWeapon.range = weaponData.Range.Value;
            equippedWeapon.hitDelay = weaponData.HitDelay.Value * weaponData.AttackSpeed.Value / 100;
        end;
    end;
    local function weaponChanged(w)
        calculate(w);
        connect(w.ItemID.Changed, function() calculate(w) end);
    end;
    
    local clientEquipped = client.stats.Equipped;
    quick.each(getChildren(clientEquipped), function(v) if string_find(v.Name, 'Wep') then weaponChanged(v) end end);
    connect(clientEquipped.ChildAdded, function(v)
        if string_find(v.Name, 'Wep') then
            weaponChanged(v);
        end;
    end);
end;

-- remotes&attacking.
local remotes = replicatedStorage.Remotes;
local action = remotes.Action;

local meleeAttack, useAbility = action.MeleeAttack, action.ActivateAbility;
local dungeonVoting = remotes.UI.EndDungeon.EndOfDungeonVote;

-- classes.
local hostile = ({params=(function()
    local _ = OverlapParams.new()
        _.FilterDescendantsInstances = { workspace.NPCS }
        _.FilterType = Enum.RaycastFilterType.Whitelist;
    
    return _;
end)(), filtered = {
    ['GoblinBashWatermelon'] = true
}}); do
    function hostile:group()
        local parts = quick.map(getPartsByRadius(workspace, root.Position, equippedWeapon.range, self.params), function(p)
            return p.Parent;
        end);
        
        return quick.uniq(quick.filter(parts, function(p) return not self.filtered[p.Name] and p.Parent == workspace.NPCS end));
    end;
    
    function hostile:nearest()
        local d, h = math.huge;
        for _, v in next, getChildren(workspace.NPCS) do
            if self.filtered[v.Name] or (not findFirstChild(v, 'HumanoidRootPart')) then continue end;

            local magnitude = distanceFromCharacter(client, v.HumanoidRootPart.Position);
            if magnitude <= d then
                d = magnitude;
                h = v;
            end
        end;
        
        return h, d;
    end;
end;

local ability = {}
do
    function ability.cooldown(a)
        local attributes = findFirstChild(character, 'Attributes');
        return attributes and
            findFirstChild(attributes.Value, 'Cooldowns') and findFirstChild((attributes.Value).Cooldowns, string_format('Wep%dAbilityCD', a)) ~= nil;
    end;
    
    function ability:use(a)
        if (not self.cooldown(a)) then
            task.spawn(fireServer, useAbility, a);
            return true;
        end;
        return false;
    end;
end;

local function attack(group)
    local targetMap = #group > 0 and quick.map(group, function(v)
        return { TargetCharacter = v, KnockbackDirection = cf(v3(root.Position.X, v.HumanoidRootPart.Position.Y, root.Position.Z), v.HumanoidRootPart.Position).LookVector }
    end);
    
    if not targetMap then return end;
    for i = 1, 2 do
        local w = findFirstChild(client.stats.Equipped, 'Wep' .. i);
        if w and not blacklistedWeaponTypes[w.ItemType] then
            fireServer(meleeAttack, i, { Targets = targetMap });
            ability:use(i);
        end;
    end;
end;

-- lib.
local lib = loadstring(httpGet(game, 'https://raw.githubusercontent.com/Kinlei/MaterialLua/master/Module.lua'))()
.Load({ Title = 'Venture Tale  ―  Bleus', Style = 1, SizeX = 400, SizeY = 400, Theme = 'Aqua', ColorOverrides = { MainFrame = Color3.fromRGB(35, 35, 35) }});

local killAura;
local autoDungeon, ignore = create'BindableEvent';

local main = lib.New({
    Title = 'Main'
}); do
    main.Toggle({
        Text = 'autoDungeon (in testing)',
        Callback = function(state)
            fire(autoDungeon, state);
        end
    });

    main.Toggle({
        Text = 'killAura',
        Callback = function(state)
            killAura = state;
            while killAura do
                local hostiles = hostile:group();
                attack(hostiles);
                task.wait((#hostiles == 0 and 0) or equippedWeapon.hitDelay);
            end;
        end
    });
end;

local modify = lib.New({
  Title = 'Modify'
}); do
    local function destroyGates(v) if string_find(v.Name, 'Gate') then destroy(v) end end;
    modify.Button({
        Text = 'removeEnvironment (un-reversible)',
        Callback = function()
            quick.each(getChildren(map), destroyGates);
            connect(map.ChildAdded, function(v) task.defer(destroyGates, v) end);
            
            local segments = findFirstChild(map, 'Segments');
            if segments then destroy(segments) end;
        end
    });
end;

do
    local function dashWarp(cf)
        local res = ability:use(999);
        if res then
            task.wait(0.165);
            root.CFrame = cf;
        end;
    end;
    
    local autoDungeon_broom = broom();
    connect(autoDungeon.Event, function(state)
        if not state then return autoDungeon_broom:clean() end;
        autoDungeon_broom:GiveTask(connect(render, function()
            if not root or (humanoid and humanoid.Health == 0) then return end;
            
            local hostile, distance = hostile:nearest();
            local gate, loot = (findFirstChild(projectiles, 'WaitingForPlayers') or findFirstChild(projectiles, 'BossWaitingForPlayers')), findFirstChild(map, 'LootPrompt', true);
            -- local dungeonFailed = replicatedStorage.ControlSettings.Failed.Value;
            
            if gate then
                root.CFrame = gate.CFrame + Vector3.yAxis;
                return;
            elseif loot then
                fireproximityprompt(loot);
                fireServer(dungeonVoting, 'ReplayDungeon');
            elseif hostile then
                local waiting = findFirstChild(hostile, 'Waiting' .. hostile.Name);
                if not findFirstChild(character, 'ForceField') and (waiting or (distance >= 20 and not (distance <= equippedWeapon.range))) then
                    dashWarp(hostile.HumanoidRootPart.CFrame);
                end;
                
                humanoid.MaxSlopeAngle = math.huge;
                characterPathing:Run(hostile.HumanoidRootPart.Position + hostile.HumanoidRootPart.CFrame.lookVector * -2);
            end;
        end));
    end);

    connect(client.CharacterAdded, function(newCharacter)
        character = newCharacter;
        root = waitForChild(newCharacter, 'HumanoidRootPart');
        humanoid = waitForChild(newCharacter, 'Humanoid');
        characterPathing = pathing.new(newCharacter, {
            Costs = {
                Water = math.huge,
                Neon = math.huge
            }}, {
            TIME_VARIANCE = 0.01,
            COMPARISON_CHECKS = 2,
            JUMP_WHEN_STUCK = false
        });
    end);
end;
